import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Amplify } from "aws-amplify";
import { Auth } from "@aws-amplify/auth";
import awsExports from "./aws-exports";
Amplify.configure(awsExports);
const getBaseDomain = (domain) =>
    domain.substring(domain.lastIndexOf(".", domain.lastIndexOf(".") - 1) + 1);
const domain = getBaseDomain(window.location.hostname.toString());
console.log(domain);
Auth.currentAuthenticatedUser().then((user) => console.log(user));
Auth.currentCredentials().then((creds) => console.log(creds));
Auth.currentSession()
    .then((user) => console.log(user))
    .catch((err) => console.log(err));
// Auth.currentSession() does not currently support federated identities. Please store the auth0 session info manually(for example, store tokens into the local storage).
// Auth.configure({
//     ...Auth,
//     // authenticationFlowType: 'USER_PASSWORD_AUTH',
//     mandatorySignIn: true,
//     cookieStorage: {
//         domain: 'amplifyapp.com',
//         path: '/',
//         expires: 365,
//         sameSite: 'lax',
//         // Secure should be true, but localhost runs on http
//         secure: true
//     }
// });
console.log(Auth);
Amplify.configure({
    Auth: {
        // (required) only for Federated Authentication - Amazon Cognito Identity Pool ID
        identityPoolId: "eu-central-1:33711289-6e74-4777-8215-b4d13a463b56",

        // (required)- Amazon Cognito Region
        region: "eu-central-1",

        // (optional) - Amazon Cognito Federated Identity Pool Region
        // Required only if it's different from Amazon Cognito Region
        identityPoolRegion: "eu-central-1",

        // (optional) - Amazon Cognito User Pool ID
        userPoolId: "eu-central-1_gXrWWbLEc",

        // (optional) - Amazon Cognito Web Client ID (26-char alphanumeric string, App client secret needs to be disabled)
        userPoolWebClientId: "56dinahga8ugtqb9kjltnu5fsv",

        // (optional) - Enforce user authentication prior to accessing AWS resources or not
        mandatorySignIn: false,

        // (optional) - Configuration for cookie storage
        // Note: if the secure flag is set to true, then the cookie transmission requires a secure protocol
        cookieStorage: {
            // - Cookie domain (only required if cookieStorage is provided)
            domain: domain,
            // (optional) - Cookie path
            path: "/",
            // (optional) - Cookie expiration in days
            expires: 365,
            // (optional) - See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
            sameSite: "none",
            // (optional) - Cookie secure flag
            // Either true or false, indicating if the cookie transmission requires a secure protocol (https).
            secure: true,
        },

        // (optional) - customized storage object
        // storage: cookieStorage,

        // (optional) - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
        // authenticationFlowType: 'USER_PASSWORD_AUTH',

        // (optional) - Manually set key value pairs that can be passed to Cognito Lambda Triggers
        clientMetadata: { myCustomKey: "myCustomValue" },

        // (optional) - Hosted UI configuration
        // oauth: {
        //   domain: 'your_cognito_domain',
        //   scope: [
        //     'phone',
        //     'email',
        //     'profile',
        //     'openid',
        //     'aws.cognito.signin.user.admin'
        //   ],
        //   redirectSignIn: 'http://localhost:3000/',
        //   redirectSignOut: 'http://localhost:3000/',
        //   clientId: '1g0nnr4h99a3sd0vfs9',
        //   responseType: 'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
        // }
    },
});

console.log(Amplify);
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
